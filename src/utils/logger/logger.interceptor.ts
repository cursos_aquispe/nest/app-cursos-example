import { CallHandler, ExecutionContext, Injectable, NestInterceptor } from '@nestjs/common';
import { Observable } from 'rxjs';
import {tap} from 'rxjs/operators'

@Injectable()
export class LoggerInterceptor implements NestInterceptor {
  intercept(context: ExecutionContext, next: CallHandler): Observable<any> {
    //console.log('Before.....', context['contextType']);
    const [req,res] = context.getArgs();
    console.log('Before.....', req.params);
    console.log('Before.....', req.ip);
    return next.handle().pipe(tap((value)=>console.log('respuesta pipe   ',value)))
  }
}
