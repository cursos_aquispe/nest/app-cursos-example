import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { ValidationPipe, VersioningType } from '@nestjs/common';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { json } from 'express';

async function bootstrap() {
  const app = await NestFactory.create(AppModule,{
    cors:true  // activar cors
  });


  app.use(json({limit:'60mb'})); // limite del paylod en los endpoint

  // verisonar el codigo
  app.enableVersioning({
    defaultVersion:'1',
    type: VersioningType.URI
  })
  

  // ==== swager ==========
  const config = new DocumentBuilder()
    .addBearerAuth()
    .setTitle('Documentación API')
    .setDescription('Documentación Api Descripción')
    .setVersion('1.0')
    .addTag('Courses')
    .addTag('Videos')
    .addTag('Awards')
    .addTag('Auth')
    .build();
  const document = SwaggerModule.createDocument(app, config);
  SwaggerModule.setup('doc', app, document);
  // fin swager

  //console.log('____ENV______ ',process.env.PORT)
  app.useGlobalPipes(new ValidationPipe()); // validaciona nivel de todo el proyecto
  await app.listen(3000);
}
bootstrap();
