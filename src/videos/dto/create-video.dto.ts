import { IsNotEmpty, IsUrl, Length } from "class-validator";

export class CreateVideoDto {
    @IsNotEmpty()
    @Length(1,50)
    title:string;

    @IsNotEmpty()
    @Length(1,50)
    description:string;

    @IsUrl()
    src:string;
}


/*{
    "title":"Video 1 NESTJS",
    "description":"Aprendiento NEST",
    "src":"hhht7youtube.com.bo/sdsd"
  }*/