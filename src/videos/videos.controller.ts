import { Controller, Get, Post, Body, Patch, Param, Delete, Query, UsePipes, 
  ValidationPipe, 
  UseInterceptors,
  UploadedFile} from '@nestjs/common';
import { VideosService } from './videos.service';
import { CreateVideoDto } from './dto/create-video.dto';
import { UpdateVideoDto } from './dto/update-video.dto';
import { ApiTags } from '@nestjs/swagger';
import { LoggerInterceptor } from 'src/utils/logger/logger.interceptor';
import { FileInterceptor } from '@nestjs/platform-express';
import { storage } from 'src/utils/media.handle';
import { CoursesService } from 'src/courses/courses.service';

@Controller('videos')
@ApiTags('Videos')
@UseInterceptors(LoggerInterceptor) // usara interceptor
// @UsePipes(new ValidationPipe()) validacion solo a nivel de este controller
export class VideosController {
  constructor(
    private readonly videosService: VideosService,
    private readonly coursesService: CoursesService
  
  ) {}

  @Post()
  create(@Body() createVideoDto: CreateVideoDto) {
    console.log(createVideoDto);
    return this.videosService.create(createVideoDto);
  }

  @Post('upload')
  @UseInterceptors(FileInterceptor('avatar',{storage}))
  handleUpload(@UploadedFile() file: Express.Multer.File) {
    console.log("aRCHIVOS ENTRANDO A CONTROLLER");
    console.log(file);
  }

  @Get()
  findAll(@Query('id') id:string) {
    return this.videosService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    console.log('desde controller que tengo aqui? ',id);
    return this.videosService.findOne(+id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateVideoDto: UpdateVideoDto) {
    return this.videosService.update(+id, updateVideoDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.videosService.remove(+id);
  }
}
