import { Body, Controller, Post } from '@nestjs/common';
import { AuthService } from './auth.service';
import { ApiTags } from '@nestjs/swagger';
import { RegisterAuthDto } from './dto/register-auth.dto';
import { LoginAuthDto } from './dto/login-auth.dto';

@Controller('auth')
@ApiTags('Auth') // con esto se casa con sus agrupados q estan configurados en main.ts
export class AuthController {
  constructor(private readonly authService: AuthService) {}



  @Post('register')
  handelRegistrar(@Body() registerBody: RegisterAuthDto){
    //console.log(registerBody)
    return this.authService.register(registerBody);
  }

  @Post('login')
  handelLogin(@Body() loginBody: LoginAuthDto){
    return this.authService.login(loginBody);
  }

}
