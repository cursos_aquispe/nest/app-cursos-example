import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { User, UserDocument } from 'src/users/model/user.schema';
import { UsersModule } from 'src/users/users.module';
import { RegisterAuthDto } from './dto/register-auth.dto';
import { compareHash, generateHash } from './utils/handleBcrypt';
import { LoginAuthDto } from './dto/login-auth.dto';
import { JwtService } from '@nestjs/jwt';

@Injectable()
export class AuthService {
    constructor(
        private readonly jwtService:JwtService,
        @InjectModel(User.name) private readonly userModel:Model<UserDocument>
    ){}

    public async login(userLoginBody:LoginAuthDto){
        const {password} = userLoginBody
        const userExist = await this.userModel.findOne({email:userLoginBody.email});
        if(!userExist){
            throw new HttpException('Not Found',HttpStatus.NOT_FOUND)
        }
        const isCheck = await compareHash(password,userExist.password)
        if(!isCheck){
            throw new HttpException('Passwrd Invalido',HttpStatus.CONFLICT)
        }
        const  userFlat =  userExist.toObject();
        delete userFlat.password;

        const payload ={
            id:userFlat._id,
        }
        const token = this.jwtService.sign(payload)
        const data = {
            token,
            user:userFlat
        }

        return  data;
    }

    public async register (userBody:RegisterAuthDto){
        const {password, ...user} = userBody;
        const userParse = {
            ...user,
            password:await generateHash(password)
        };
        console.log(userParse);
        return this.userModel.create(userParse);
    }
}
