import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import mongoose, { Document } from 'mongoose';

export type AwardsDocument = Awards & Document;

@Schema()
export class Awards {
  @Prop()
  title: string;

  @Prop()
  idUser: mongoose.Types.ObjectId;

  @Prop()
  idAutor: mongoose.Types.ObjectId;

  @Prop()
  description: string;

  @Prop()
  cover:string
}

export const AwardsSchema = SchemaFactory.createForClass(Awards);
