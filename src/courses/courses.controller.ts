import { Controller, Get, Post, Body, Patch, Param, 
  Delete, HttpCode, HttpException, HttpStatus, 
  ParseIntPipe, UseGuards, Req } from '@nestjs/common';
import { CoursesService } from './courses.service';
import { CreateCourseDto } from './dto/create-course.dto';
import { UpdateCourseDto } from './dto/update-course.dto';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { SlugPipe } from './pipes/sglu/sglu.pipe';
import { BrowserAgentGuard } from 'src/guards/browser-agent/browser-agent.guard';
import { JwtGuardGuard } from 'src/guards/jwt-guard/jwt-guard.guard';
import { Request } from 'express';


@Controller('courses')
@ApiTags('Courses')
//@UseGuards(BrowserAgentGuard) // pdira token
@UseGuards(JwtGuardGuard) // pdira token
//@ApiBearerAuth()   para swager  auth a nivel controlador
export class CoursesController {
  constructor(private readonly coursesService: CoursesService) {}

  @Post()
  @HttpCode(201) // definir respuesta por defecto 
  create( @Req() req:Request, @Body() createDto: CreateCourseDto) {
    console.log(req.user)
    return this.coursesService.create(createDto);
  }
  @Get(':title')
  getDetail(@Param('title',new SlugPipe()) title:string){
    return this.coursesService.findOne(1);
  }
  
}
