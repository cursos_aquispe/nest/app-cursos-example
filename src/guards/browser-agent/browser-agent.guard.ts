import { CanActivate, ExecutionContext, HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { Observable } from 'rxjs';

@Injectable()
export class BrowserAgentGuard implements CanActivate {
  canActivate(
    context: ExecutionContext,
  ): boolean | Promise<boolean> | Observable<boolean> {
    const req = context.getArgByIndex(0)
    const userAgent = req.headers['user-agent']
    const isAllowed = (userAgent==='google/chrome')
    if(!isAllowed){
      throw new HttpException('BROWSER AGENT IVALID',HttpStatus.BAD_REQUEST)
    } 
    return true;
  }
}
